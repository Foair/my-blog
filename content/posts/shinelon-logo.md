---
title: "炫龙毒刺 X6 开机 BIOS logo 更换（刷 BIOS）"
date: 2018-06-19T22:57:12+08:00
draft: false
categories: ["文章"]
---

## 模具说明

毒刺 X6 的准确模具是：`CLEVO:N150RF-1.05.06TSZ1`，简单写是 `N150RF`，并非是我以前错认为的 `N151RD` 或 `N151RD1`。

根据 http://www.sohu.com/a/109420729_234705 的信息，可以知道 N150RF 是 N151RD 的改款。下载驱动选择 `N15xRD1/N17xRD1 Series`。

驱动下载地址：http://www.clevo.com.tw/clevo_down.asp?lang=cn

快捷下载：[这里](http://www.clevo.com.tw/en/e-services/download/ftpOut.asp?Lmodel=N1xxRF&ltype=9)。

## 工具

AMI 板子官方更新工具（AFU）：[这里](https://ami.com/en/download-license-agreement/?DownloadFile=AMIBIOS_and_Aptio_AMI_Firmware_Update_Utility.zip)。

AMI 官方工具组件：https://ami.com/en/products/bios-uefi-tools-and-utilities/bios-uefi-utilities/，除了 AFU 以外好像都不提供免费下载。

AFU 中有对多个版本 BIOS 的支持，比如 Aptio V、Aptio 4 和 AMIBIOS 8，通过查看我的需要 Aptio V 版本的 AFU。AFU 的更新工具有在 Windows 平台的 GUI 界面，也有 DOS 的程序，按需求使用即可。

AMI 官方工具组件包中有很多有用的东西，比如 AMIBCP 可以对 BIOS 某些选项是否可见进行设置。Change Logo 可以更换 BIOS ROM 文件中的 logo，这样就可以定制 OEM 或者自己的 logo。

对于这些工具，由于官方没有免费提供，只能到网上找到一个比较新的版本。如果版本过低的话可能遇到一些问题，比如 Chango Logo 的低版本对 Aptio V 的 ROM 就不支持，提示找不到图像。

## 蓝天原版 BIOS

BIOS 下载：https://repo.palkeo.com/clevo-mirror/N1xxRF/

分别下载最新的 EC 和 BIOS，注意看日期和说明。（一般下面的才是最新的）

使用 DOS 工具箱才能进行刷写 BIOS。进入 DOS 系统之后，默认位置是在 `A:`，`C:` 应该是第一个可以识别的 FAT32 格式的分区。所以将 BIOS 文件提前放到优盘中，使用 `CD` 切换目录。

关闭 UEFI，这样才能进入 DOS 系统。

先刷最新的 EC，有些模具提供了 2 个 EC，这两个 EC 都要按照顺序进行刷入。然后刷入 ROM。

进入 EC 所在的目录，使用 `ecflash` 刷入（如果有两个，分别刷入即可）。

进入 ROM 所在的目录，输入 `meset`，然后会自动重启。

再次进入 ROM 所在的目录，输入 `flashme` 进行刷入，然后会自动关机。

如果以上步骤没有自动关机或者重启，DOS 中使用 `Ctrl + Alt + Delete` 关机。

## 自定义 BIOS

除了 Change Logo 和 AMIBCP 以外，[UEFITool](https://github.com/LongSoft/UEFITool) 也可以修改 ROM，具体是通过相关查找相关 GUID 进行 ROM 的修改。

Change Logo 可以修改 BIOS ROM 中的 logo，第一个是 UEFI 模式下的 logo，第二个是 Legacy 模式下的 logo，第三个是 Intel 的 logo，还有几个是相关热键的图片。一般 UEFI 模式下不会显示相关热键，所以直接修改前三个图像就可以了。据说 BIOS 可以支持 24 位的 BMP 和 JPG 图像，可以使用 1080P 分辨率的图像。将 Intel 的 logo 替换为 1 像素的黑色就可以「隐藏」掉 Intel 的标识。

写入修改 BIOS 的方法：`fpt -f N150RF.BIN -bios`。

## 提示

参考链接中有更多内容，包括如何恢复 BIOS。

## 参考

【图片】神舟Z7M-SL7D2刷BIOS的方法以及一些对它的想法【笔记本吧】_百度贴吧
https://tieba.baidu.com/p/4625463595

【图片】蓝天刷BIOS教程【义索科技吧】_百度贴吧
https://tieba.baidu.com/p/4295689561?red_tag=2284682365

【图片】神舟修改LOGO，刷BIOS及自救教程，系统下载【神舟笔记本吧】_百度贴吧
https://tieba.baidu.com/p/3243912429?red_tag=b3573781089

[补充贴]关于用changelogo无法替换logo时的手动替换方法【准系统吧】_百度贴吧
https://tieba.baidu.com/p/4941767571

【图片】K650刷机BIOS更新教程和各类问题【神舟笔记本吧】_百度贴吧
https://tieba.baidu.com/p/3100999613

【图片】【教程】自制解锁bios，加slic，改logo和添加序列号【准系统吧】_百度贴吧
https://tieba.baidu.com/p/4936939406

P157SMA刷BIOS更改开机画面终极帖（下）【教程+工具+注意事项】【叔叔左左吧】_百度贴吧
https://tieba.baidu.com/p/3588226687

【图片】改开机LOGO教程以及DMI 详细到爆炸【温州义索吧】_百度贴吧
https://tieba.baidu.com/p/3911889035

我对“AMI的BIOS刷失败的完美恢复方法”的实践 | 好运博客
http://www.lucktu.com/archives/606.html

修改uefi启动的logo画面（AMI的bios）_百度经验
https://jingyan.baidu.com/article/6f2f55a1830aabb5b83e6c7a.html

如何更换Windows 10的启动logo
https://zhuanlan.zhihu.com/p/28201533
