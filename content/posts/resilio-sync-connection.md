---
title: "Resilio Sync 无法连接解决方法"
date: 2018-06-19T21:56:20+08:00
draft: false
categories: ["文章"]
---

## 修改 HOSTS 文件

首先修改好本地 HOSTS 文件，添加以下行来抵抗 DNS 污染。

```text
## Resilio Sync
54.192.232.168 config.resilio.com config.getsync.com config.usyncapp.com
```

如果上面的 IP 已经不能 `ping` 通，那么可以查询最新的 IP 地址：[Ping - IPIP.NET](https://www.ipip.net/ping.php)。

Resilio Sync 通过地址 `http://config.getsync.com/sync.conf` 下载配置文件来获得中央服务器信息。

而这个域名已经被污染（屏蔽）了，无法下载到配置文件，通过修改上面的 HOSTS 文件即可实现下载文件。

## 使用 SOCKS 5 代理

`sync.conf` 文件中的 tracker 服务器 IP 几乎被完全屏蔽，需要经过代理服务器，才能访问到 tracker。

由于 Resilio Sync 没有选项可以单独设置 tracker 的代理，全局设置的话又会导致所有流量经过代理。所有流量经过代理主要有两个弊端：代理服务器的流量一般都是有限制的；可能遭到 DMCA 投诉导致代理被封。

所以使用 Proxifier 仅仅代理 tracker 的 IP 就可以了。

在 `sync.conf` 中只需要代理 `trackers` 中的 IP 地址，`relays` 和 `mobile_push_proxies` 等里面的地址不用管。

```text
{
    "trackers": [
        {
            "addr": "173.244.217.42:4000",
            "addr6": "[2606:2e00:8003:1:ec4:7aff:fe57:108e]:4000"
        },
        {
            "addr": "209.95.56.60:4000",
            "addr6": "[2606:2e00:0:7c:225:90ff:fe47:76a6]:4000"
        }
    ],
    "relays": [
        {
            "addr": "107.182.230.198:3000",
            "addr6": "[2606:2e00:8003:a::8]:3000"
        },
        {
            "addr": "107.182.230.198:3001",
            "addr6": "[2606:2e00:8003:a::8]:3001"
        },
        {
            "addr": "173.244.209.150:3000",
            "addr6": "[2606:2e00:0:89::7]:3000"
        },
        {
            "addr": "173.244.209.150:3001",
            "addr6": "[2606:2e00:0:89::7]:3001"
        }
    ],
    "mobile_push_proxies": [
        {
            "addr": "54.235.182.157:3000"
        }
    ]
}
```

打开 Proxifier，选菜单「配置」-「代理规则」。点「新建」按钮，建立一条规则。不用填「应用程序」和「目标端口」，只要在「目标主机」那里把那一堆 IP 地址填上即可。如：`173.244.217.42; 209.95.56.60`。

注意，这里只填 `trackers` 的 IP 地址，不填 `relays` 的。否则传输流量都从代理服务器走了，影响速度。

「配置」-「代理服务器」用于设置代理服务器，可以是任何代理程序（SS、SSR、V2Ray）。

再进一次「代理规则」，把默认的方式设置为 `Direct`，把刚刚新建的代理设置为代理服务器。

## Proxifier ￥

Proxifier 注册码（3 个）

```text
5EZ8G-C3WL5-B56YG-SCXM9-6QZAP
G3ZC7-7YGPY-FZD3A-FMNF9-ENTJB
YTZGN-FYT53-J253L-ZQZS4-YLBN9
```

其他版本

```text
L6Z8A-XY2J4-BTZ3P-ZZ7DF-A2Q9C (Portable Edition)
P427L-9Y552-5433E-8DSR3-58Z68 (Mac)
```

& [Proxifier 注册码 - 百度知道](https://zhidao.baidu.com/question/434483290434627164.html)

## 参考链接

* [Resilio Sync 无法获取追踪器列表 - 知乎](https://www.zhihu.com/question/60919926)
* [Resilio Sync 无法获取追踪器列表 - 微力同步](http://verysync.com/tutorial/fix-resilio-sync-china-lost.html)
* [Resilio Sync 无法获取追踪器列表 - 微力同步](http://verysync.com/tutorial/fix-cannot-get-the-list-of-trackers.html)
* [Resilio Sync 无法获取跟踪服务器列表 - Zephyr](https://www.zephyr.vip/818)
