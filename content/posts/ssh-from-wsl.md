---
title: 从 WSL 子系统连接 SSH
date: 2020-05-19T13:46:00+08:00
draft: true
---

网上有许多使用 XShell 连接 SSH 的文章，但 Windows 提供了相应的替代品：Windows Terminal 和 Windows Subsystem Linux。配合这两者可以得到胜过 Xshell 的体验。
