---
title: "Office 字体嵌入的问题"
date: 2018-06-19T22:50:53+08:00
draft: false
categories: ["文章"]
---

在 PowerPoint 中无法嵌入 OpenType 类型的字体（Word 没有测试过），会出现一个警告。可以正常嵌入 TrueType 的字体。

除此之外，还有一个字体许可证的问题。一些字体由于字体许可证的限制，也可能无法嵌入 Office 的文件中。

字体许可证的可嵌入性可能有 4 种取值：

`可安装`：允许被嵌入到文档中，并且可以永久安装在计算机中；
`可修改`：允许被嵌入到文档中，但只能临时地安装在计算机中；
`打印和预览`：允许被嵌入到文档中，但只能为了打印目的而被临时安装在计算机中；
`受限`：不允许被嵌入到文档中。

& https://www.itg.ias.edu/content/embedding-fonts-microsoft-word-documents-windows
& https://www.adobe.com/products/type/font-licensing/font-embedding-permissions.html
& http://www.pptfaq.com/FAQ00076_Embedding_fonts.htm
& https://support.microsoft.com/en-us/help/908475/you-cannot-embed-an-adobe-opentype-font-in-a-document-in-an-office-pro
