---
title: 使用 Hugo 和 Netlify 构建博客
date: 2020-05-06T20:14:31+08:00
draft: false
---

## 创建第一篇文章

通过 GitHub Releases 获得最新的 Hugo 二进制文件，并将 Hugo 添加到环境变量中：

```powershell
$env:Path += ';X:\hugo_0.70.0_Windows-64bit'
hugo version
```

创建新一个新的站点，如我的博客 `foair.me`：

```powershell
hugo new site foair.me && cd foair.me
```

Hugo 没有任何内置的主题，因此需要在添加用于 Hugo 的主题。

```powershell
cd themes
git clone https://github.com/vividvilla/ezhil.git
```

创建一篇新的文章，默认格式是 Markdown 格式。

```powershell
hugo new posts/blogging-with-hugo-and-netlify.md
```

此时可以使用 `hugo` 命令将 Markdown 格式转换成 HTML 格式的静态网站。也可以使用 `hugo server` 子命令开启一个本地服务器，用于预览网站，以便看到最终效果。

在 Markdown 文件的首部会有类似的信息，其被称作 front matter，用于标记 Markdown 文档的元信息。如文档的标题、创建时间、修改时间等信息。其中 `draft` 字段若为 `true` 用于标明此文档是草稿，在发布和预览的时候默认都不显示。对于草稿来说，也可以使用 `-D` 选项将其显示出来，如在本地预览草稿：

```powershell
hugo server -D
```

## 网站配置与主题配置



## 部署上线

部署上线的方式可以有许多种，如 GitHub 提供的 GitHub Action、GitLab 提供的 CI/CD 服务都可以将网站上线和部署。也可以在本地转换之后将静态文件上传到服务器中。这里主要介绍使用 Netlify 提供部署服务。
