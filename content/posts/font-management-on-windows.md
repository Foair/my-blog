---
title: "在 Windows 上的字体管理"
date: 2018-07-29T14:43:11+08:00
draft: false
---

在重装之后需要安装系统中没有的字体，以便在各种程序中使用。然而，安装字体可能遇到如下问题：

* 字体安装过多可能拖慢字体选择框的速度，占用过多的空间
* 可能已经安装过这个字体了（有时会在注册表中增加一条完全没有用记录）；
* 可能安装字体的版本较新，而没有覆盖掉老版本；
* 手动右键安装字体的方法费时费力；
* 有时只需要临时安装几个字体，而不想安装之后又手动卸载。

于是我先在 `G:\Files\` 下创建了几个文件夹，用于存放字体。

比如 `Chinese`，在 `Chinese` 中又创建了几个目录，分别表示不同字体厂商。

- `Founder`（方正字库）
- `HANYI`（汉仪字库）
- `MakeFont`（造字工房）
- `Senty`（新蒂字体）
- `WenYue`（文悦字型）

又如在 `Founder` 中，创建了 `Archive` 文件夹，用来存放暂时不安装的字体。

```
Archive/
FZFSK.TTF
FZHTK.TTF
FZJLJW.TTF
FZKTK.TTF
FZLBK.TTF
FZLSK.TTF
FZLTSK.TTF
FZNBSJW.TTF
FZNHT.TTF
...
```

在 `Preinstall.bat` 中的内容如下：

```bat
@ECHO OFF
TITLE 字体一键安装

ECHO 确认以管理员身份运行
PAUSE

CD /D %~dp0

REM Adobe 字体
ECHO 正在安装 Adobe 宋体、Adobe 仿宋等字体...
XCOPY "%~dp0..\Adobe\Chinese" "%SystemRoot%\Fonts" /D /Y
ECHO 正在安装 Adobe Garamond Pro...
XCOPY "%~dp0..\Adobe\GaramondPro" "%SystemRoot%\Fonts" /D /Y
ECHO 正在安装 Adobe Source Han 字体...
XCOPY "%~dp0..\Adobe\SourceHan" "%SystemRoot%\Fonts" /D /Y
ECHO 正在安装 Adobe Source Code Pro 字体...
XCOPY "%~dp0..\Adobe\SourceCodePro" "%SystemRoot%\Fonts" /D /Y
ECHO 正在安装 Adobe Source Serif Pro 字体...
XCOPY "%~dp0..\Adobe\SourceSerifPro" "%SystemRoot%\Fonts" /D /Y

REM 新蒂字体
ECHO 正在安装新蒂字体...
XCOPY "%~dp0..\Chinese\Senty" "%SystemRoot%\Fonts" /D /Y

REM 方正字体
ECHO 正在安装方正字体...
XCOPY "%~dp0..\Chinese\Founder" "%SystemRoot%\Fonts" /D /Y

REM 汉仪字体
ECHO 正在安装汉仪字体...
XCOPY "%~dp0..\Chinese\HANYI" "%SystemRoot%\Fonts" /D /Y

REM 造字工房字体
ECHO 正在安装造字工房字体...
REM XCOPY "%~dp0..\Chinese\MakeFont" "%SystemRoot%\Fonts" /D /Y

REM 文悦字体
ECHO 正在安装文悦字体...
REM XCOPY "%~dp0..\Chinese\WenYue" "%SystemRoot%\Fonts" /D /Y

REM 西文字体
ECHO 正在安装西文字体...
XCOPY "%~dp0..\Western\MyriadPro" "%SystemRoot%\Fonts" /D /Y
XCOPY "%~dp0..\Western\MinionPro" "%SystemRoot%\Fonts" /D /Y
XCOPY "%~dp0..\Western\FiraCode" "%SystemRoot%\Fonts" /D /Y
XCOPY "%~dp0..\Western\CronosPro" "%SystemRoot%\Fonts" /D /Y
XCOPY "%~dp0..\Western\Gravity" "%SystemRoot%\Fonts" /D /Y

REM XCOPY "" "%SystemRoot%\Fonts" /D /Y

ECHO 使用 FontReg 刷新注册表...
FontReg

ECHO 在 Generate.py 中修改时间为当前时间

ECHO 完成！
PAUSE > NUL
```

这个批处理的目的是将特定文件夹的直接文件拷贝到 `%SystemRoot%\Fonts` 文件夹。光是复制字体过去是没有效果，还需要添加相应的注册表项才能有效果。这里直接调用了 FontReg 这个工具，进行注册表的刷新，它能够删除注册表中的冗余项，也可以为新字体添加注册表项。

FontReg 的源代码和下载在 http://code.kliu.org/misc/fontreg/ 可以看到。

在有些动漫或电影资源的目录里，有时可以看到附带有字体文件，这些字体文件可以安装在系统中，但是完全没有必要。一是这些字体的质量可能比较差（字体轮廓锯齿问题、字体版本过低问题），二是自己根本可能使用不到这些字体，因此最好是要用的时候在进行加载，这时就可以使用 FontLoader 加载字体，仅仅在需要用的使用，比如播放视频的时候。这样就不需要经过安装字体、卸载字体的繁琐过程。

FontLoader 也是开源的，可以在 https://bitbucket.org/cryptw/fontloader 下载源代码和目标文件。

的确，可能有第三方软件可能具有这些功能，但是为了这些小功能请出一个软件完全是不必要的。别人软件的工作流程是不可控制的，只有自己清楚地明白其中的道理才是真正有意义的。另外，其他软件提供的字体版本很有可能不是最新的，而最新版本的字体是需要自己收集的。

另外两个软件，供参考：

[字由](http://www.hellofont.cn/download) / [NexusFont](http://www.xiles.net/) / [字+](http://www.foundertype.com/index.php/Index/ftXplorer)

